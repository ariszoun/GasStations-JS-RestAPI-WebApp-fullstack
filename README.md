# GasStations app with Google Maps

## Web application for helping individuals find the geolocation of various petrol stations near them and check prices.

### Technologies used in this APP:
- Frontend: HTML, CSS & Bootstrap, Javascript, and "ejs" engine template.
- Backend: Node.js, Express.js, MySQL database.
- Authentication and Authorization implemented with Passport-Local. Bcryptjs is used for encryption.

### To Run the application:
- install nodejs and npm
- npm install
- npm install -g nodemon
- To run locally: >nodemon server.js
- Run without nodemon: >node server.js

### WARNING: Import a: "google api key=YOUR_API_KEY" - for the map in views -> index.ejs -> line 272